// ==UserScript==
// @name         Stats NAW
// @namespace    http://tampermonkey.net/
// @version      0.1
// @downloadURL  https://gitlab.com/camill_a/naw-scripts/-/raw/main/stats.user.js
// @updateURL    https://gitlab.com/camill_a/naw-scripts/-/raw/main/stats.user.js
// @description  Stats d'alliance
// @author       Nabes
// @match        https://*.natureatwar.fr/*
// @icon         https://www.google.com/s2/favicons?domain=natureatwar.fr
// @grant        none
// ==/UserScript==

let modal;
let historic = null;
const dates = [];
const members = [];
const idxMap = ['name', 'type', 'exp', 'food', 'wood', 'water', 'date'];
const filteredTypes = ['Convois', 'Flood', 'Chasse', 'Activité']

const tableToArray = () => $('#informationDon tbody tr').slice(1).map((idx, row) => {
    const childs = row.children;
    let obj = {};
    for (let i = 0; childs.length !== i; i++) {
        obj[idxMap[i]] = childs[i].innerHTML;
    }
    return obj;
}).toArray();

const parseHistoric = () => {
    historic.forEach(obj => {
        const date = obj.date.split('le ')[1];
        if (!dates.includes(date)) {
            dates.push(date);
        }
        if (!members.includes(obj.name)) {
            members.push(obj.name);
        }
    });
};

const fillTable = () => {
    const date = $('#dateSelector').val();
    const list = historic.filter(o => o.date.includes(date));
    const membersData = members.reduce((obj, member) => ({...obj, [member]: {Convois: 0, Flood: 0, Chasse: 0, exp: 0, connected: false}}), {});
    list.forEach(data => {
        const memberData = membersData[data.name];
        if (filteredTypes.includes(data.type)) {
            if (data.type === 'Activité') {
                memberData.connected = true;
            } else {
                memberData[data.type] += 1
            }
            memberData.exp += Number(data.exp.split(' ').join(''));
        }
    });
    $('#histoTableBody').empty().append(members.map(name => {
        const memberData = membersData[name];
        return `<tr class="${memberData.connected ? '' : 'bg-danger'}"><td>${name}</td><td>${memberData.Flood}</td><td>${memberData.Convois}</td><td>${memberData.Chasse}</td><td>${memberData.exp}</td></tr>`;
    }));
}

const createTable = () => {
    $('#modal').append(`<table class="table table-hover table-bordered"><thead><tr><th>Pseudo</th><th>💂‍♂️</th><th>🏡</th><th>🕷️</th><th>✨</th></tr></thead><tbody id="histoTableBody"></tbody></table>`)
}

const createModal = () => {
    $('body').append(`<div id="modal" style="font-size: large;"><div class="form-group"><select class="form-control" id="dateSelector"></select></div></div>`);
    createTable();
    $('#modal').hide();
}

const setupModal = () => {
    $('head').append('<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>');
    createModal();
    if (window.location.pathname === '/quetealliance') {
        historic = tableToArray();
        window.localStorage.setItem('memberHistory', JSON.stringify(historic));
    } else {
        const stringHistory = window.localStorage.getItem('memberHistory');
        historic = stringHistory ? JSON.parse(stringHistory) : null;
    }
    if (historic !== null) {
        parseHistoric();
        $('#nawddonLeft').append('<button class="btn btn-info" id="historyModalTrigger">Historique détaillé</button>');
        $('#historyModalTrigger').click(() => {
            modal = $('#modal').dialog({
                resize: true,
                resizable: false,
                draggable: true,
                title: 'Historique détaillé'
            });
            $('#modal').show();
        });
        $("#dateSelector").append(dates.map(date => `<option value=${date}>${date}</option>`));
        fillTable();
        $('#dateSelector').on('change', fillTable);
    }
}

const createNavLeft = () => {
    $('.sideLeft .contenunavleft').after('<div class="contenusousnavleft"><div class="interieur"><center>Nawddons</center><div id="nawddonLeft"></div></div></div>')
}

const init = () => {
    $('head').append('<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>');
    setupModal();
}

const interface = () => {
    createNavLeft();
    init();
}

interface();