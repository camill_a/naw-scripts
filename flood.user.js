// ==UserScript==
// @name         Flood à la seconde
// @namespace    http://tampermonkey.net/
// @version      2.8
// @downloadURL  https://gitlab.com/camill_a/naw-scripts/-/raw/main/flood.user.js
// @updateURL    https://gitlab.com/camill_a/naw-scripts/-/raw/main/flood.user.js
// @description  Flood à la seconde près sur NaW (IL FAUT RESTER SUR LA PAGE LE TEMPS DU FLOOD)
// @author       Nabes
// @match        https://www.natureatwar.fr/attaques*
// @match        https://s1.natureatwar.fr/attaques*
// @icon         https://www.google.com/s2/favicons?domain=natureatwar.fr
// @grant        none
// ==/UserScript==

function getDateFromInput(input, withoutDate = false, UTC = false) {
    const [h, m, s] = input.value.split(':');

    const date = new Date();
    if (withoutDate === true) {
        date.setTime(0);
    }
    if (UTC) {
        date.setUTCHours(h);
        date.setUTCMinutes(m);
        date.setUTCSeconds(s);
    } else {
        date.setHours(h);
        date.setMinutes(m);
        date.setSeconds(s);
    }
    return date;
}

function getTodayDate() {
    const today = new Date();
    const day = today.getDate();
    const month = today.getMonth() + 1;
    const year = today.getFullYear();

    return `${year}-${month < 10 ? `0${month}` : month}-${day < 10 ? `0${day}` : day }`;
}

(function() {
    'use strict';
    const sendAttackBtn = document.querySelector('input[name="sendAttaque"]');
    const form = document.querySelector('form[name="convois"]');

    const fakeInput = document.createElement("input");
    fakeInput.name = "sendAttaque";
    fakeInput.value = "Envoyer";
    fakeInput.hidden = true;

    const title = document.createElement('h1');
    title.classList.add('ecriturepagetitre');
    title.innerText = "Envois de flood en décaler"
    const subTitle = document.createElement('div');
    subTitle.innerText = "(Merci de rester sur l'onglet le temps que le flood s'envoit)"

    // Container
    const container = document.createElement('div');
    container.classList.add('cadreCentral');
    container.classList.add('row');

    // InputsContainer
    const inputsContainer = document.createElement('div');
    inputsContainer.classList.add('col-md-6');

    // Result container
    const resultContainer = document.createElement('div');
    resultContainer.classList.add('col-md-6');

    // Line break
    const br = document.createElement('br');

    // Label for input delay
    const travelTimeLabel = document.createElement('label');
    travelTimeLabel.innerText = "Temps de trajet"

    // Input time creation delay
    const travelTimeInput = document.createElement('input')
    travelTimeInput.type = "time";
    travelTimeInput.step= "2";
    travelTimeInput.classList.add('form-control');
    travelTimeInput.style.width = '200px';

    // Label for input delay
    const inputSendLabel = document.createElement('label');
    inputSendLabel.innerText = "Heure d'arrivée"

    // Input time creation time to send
    const arrivalTimeInput = document.createElement('input')
    arrivalTimeInput.type = "time";
    arrivalTimeInput.step= "2";
    arrivalTimeInput.classList.add('form-control');
    arrivalTimeInput.style.width = '200px';

    // Date input create
    const dateInput = document.createElement('input');
    dateInput.type = 'date';
    dateInput.classList.add('form-control');
    dateInput.style.width = '200px';
    dateInput.value = getTodayDate();

    // Label for date input
    const dateInputLabel = document.createElement('label');
    dateInputLabel.innerText = "Date d'arivée"

    // Send button creation
    const sendDelayBtn = document.createElement('button');
    sendDelayBtn.classList.add('btn');
    sendDelayBtn.classList.add('btn-success');
    sendDelayBtn.style['margin-top'] = '10px';
    sendDelayBtn.innerText = "Envoyer en décaler";

    // DOM insertions
    form.after(container);

    container.appendChild(title);
    title.after(subTitle);
    subTitle.after(inputsContainer);
    inputsContainer.after(resultContainer);

    inputsContainer.appendChild(travelTimeLabel);
    travelTimeLabel.after(travelTimeInput);
    travelTimeInput.after(inputSendLabel);
    inputSendLabel.after(arrivalTimeInput);
    arrivalTimeInput.after(sendDelayBtn);
    arrivalTimeInput.after(dateInputLabel);
    dateInputLabel.after(dateInput);

    // Event handling
    sendDelayBtn.addEventListener('click', (e) => {
        // Prevent submit event
        e.preventDefault();
        e.stopPropagation();

        const now = Date.now();

        const [y, m, d] = dateInput.value.split('-');
        const travelTime = getDateFromInput(travelTimeInput, true, true);
        const arrivalTime = getDateFromInput(arrivalTimeInput);
        arrivalTime.setFullYear(y);
        arrivalTime.setMonth(Number(m) - 1);
        arrivalTime.setDate(d);

        const timeout = arrivalTime - (now + travelTime.getTime()) - 1000;
        const sendDate = new Date(arrivalTime.getTime() - travelTime.getTime() - 1000);

        const result = document.createElement('span');
        result.innerText = `Le flood sera envoyé : ${sendDate.toLocaleString()}`
        resultContainer.appendChild(result);

        if (timeout < 1) {
            alert("Tu peux pas retourner dans le temps bg");
        } else {
            setTimeout(() => {
                form.appendChild(fakeInput);
                form.submit();
            }, timeout);
        }
    });
})();